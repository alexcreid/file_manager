from django.contrib.auth.models import User
import django.db.models as django_models


def _get_user_file_path(instance, filename):
    return f'user_{instance.owner_id}/{filename}'


class FileUploadManager(django_models.Manager):

    """Default object manager for the FileUpload model."""

    def visible_to_user(self, user):
        """Retrieve all the files a user can see, public or private."""
        queryset = self.get_queryset()
        return queryset\
            .filter(django_models.Q(owner=user) |
                    django_models.Q(is_public=True))\
            .order_by('-created_at')


class FileUpload(django_models.Model):

    """Represents uploaded files."""

    objects = FileUploadManager()

    upload = django_models.FileField(
        upload_to=_get_user_file_path, verbose_name='File to upload',
        help_text='Select the file to upload')
    name = django_models.CharField(
        max_length=255, help_text='The original name of the file')
    owner = django_models.ForeignKey(User, on_delete=django_models.CASCADE)
    is_public = django_models.BooleanField(
        default=False, verbose_name='Make file public?',
        help_text='Public files are available to all users.')

    created_at = django_models.DateTimeField(auto_now_add=True)
    updated_at = django_models.DateTimeField(auto_now=True)
