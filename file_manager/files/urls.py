from django.urls import path

from . import views

app_name = 'files'

urlpatterns = [
    path('', views.ListFilesView.as_view(), name='list'),
    path('upload/', views.UploadFileView.as_view(), name='upload'),
    path('download/<int:file_id>/', views.DownloadFileView.as_view(),
         name='download'),
    # We'd probably want to split API URLs into their own included URLconf
    # here if we had more than just the one:
    path('api/search/', views.FileSearchAPIView.as_view(), name='api_search')
]
