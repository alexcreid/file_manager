"""Tests for the files app."""

from itertools import count

from django.contrib.auth.models import User
import django.test

from . import models

# This is just a handful of tests for demonstration purposes, we'd want much
# more coverage of the app in reality.


def create_user(_counter=count()):
    return User.objects.create_user(username=f'test_user_{next(_counter)}')


class FileUploadManagerTests(django.test.TestCase):

    """Tests for the FileUpload default model manager."""

    def test_user_can_see_own_private_files(self):
        """
        The `visible_to_user` manager method includes the user's own, non-
        public files.
        """
        user = create_user()

        models.FileUpload.objects.create(upload='fake/file/path',
                                         name='Test File 1', owner=user,
                                         is_public=False)
        models.FileUpload.objects.create(upload='fake/file/path2',
                                         name='Test File 2', owner=user,
                                         is_public=False)

        qs = models.FileUpload.objects.visible_to_user(user)

        self.assertEqual([f.name for f in qs], ['Test File 2', 'Test File 1'])

    def test_user_can_see_public_files(self):
        """
        The `visible_to_user` manager method includes the user's own public
        files as well as those of other users.
        """
        user = create_user()
        other_user = create_user()

        models.FileUpload.objects.create(upload='fake/file/path',
                                         name='My Public File', owner=user,
                                         is_public=True)
        models.FileUpload.objects.create(upload='fake/file/path2',
                                         name="Somebody's Public File",
                                         owner=other_user, is_public=True)

        qs = models.FileUpload.objects.visible_to_user(user)

        self.assertEqual([f.name for f in qs],
                         ["Somebody's Public File", 'My Public File'])

    def test_user_cannot_see_other_users_private_files(self):
        """
        The `visible_to_user` manager method does not include private files
        that were uploaded by other users.
        """
        user = create_user()
        other_user = create_user()

        models.FileUpload.objects.create(upload='fake/file/path',
                                         name='My Private File', owner=user,
                                         is_public=False)
        models.FileUpload.objects.create(upload='fake/file/path2',
                                         name="Somebody's Private File",
                                         owner=other_user, is_public=False)

        qs = models.FileUpload.objects.visible_to_user(user)

        self.assertEqual([f.name for f in qs],
                         ['My Private File'])
