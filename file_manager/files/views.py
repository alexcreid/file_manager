from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import FileResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect
from django.template.response import TemplateResponse
from django.views import View
from django.urls import reverse

from . import forms
from . import models


class UploadFileView(LoginRequiredMixin, View):

    """View which allows users to upload new files."""

    def get(self, request, *args, **kwargs):
        form = forms.FileUploadForm()
        return TemplateResponse(request, 'files/upload_file.html',
                                context={'form': form})

    def post(self, request, *args, **kwargs):
        form = forms.FileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file_upload = form.save(commit=False)
            file_upload.owner = request.user
            file_upload.name = request.FILES['upload'].name
            file_upload.save()
            return redirect('files:list')
        else:
            return TemplateResponse(request, 'files/upload_file.html',
                                    context={'form': form})


class ListFilesView(LoginRequiredMixin, View):

    """
    View which allows users to see a list of their files, plus any that are
    publically-available.
    """

    def get(self, request, *args, **kwargs):
        files = models.FileUpload.objects.visible_to_user(request.user)
        return TemplateResponse(request, 'files/list_files.html',
                                context={'files': files})


class DownloadFileView(LoginRequiredMixin, View):

    """View which allows users to download files that are visible to them."""

    def get(self, request, *args, **kwargs):
        file_id = kwargs.get('file_id')
        visible_files = models.FileUpload.objects.visible_to_user(request.user)
        file_upload = get_object_or_404(visible_files, id=file_id)

        return FileResponse(file_upload.upload, as_attachment=True,
                            filename=file_upload.name)


class FileSearchAPIView(LoginRequiredMixin, View):

    """
    An API view which allows users to search for a files whose names contain
    the given search string. Multiple strings can be provided by specifying the
    query parameter multiple times, e.g. 'q=foo&q=bar', in which case the
    only files matching all the strings will be returned.
    """

    def get(self, request, *args, **kwargs):
        search_strings = request.GET.getlist('q')

        matched_files = models.FileUpload.objects.visible_to_user(request.user)

        for s in search_strings:
            matched_files = matched_files.filter(name__icontains=s)

        result_data = [self._render_file_upload(f) for f in matched_files]

        # We have to set safe=False to allow us to render an array, but we
        # should decide how concerned we are about array constructor poisoning
        # if our clients are likely to be using ECMAScript < 5
        return JsonResponse(result_data, safe=False)

    def _render_file_upload(self, file_upload):
        return {
            'name': file_upload.name,
            'created_at': file_upload.created_at.isoformat(),
            'is_public': file_upload.is_public,
            'download_url': reverse('files:download',
                                    kwargs={'file_id': file_upload.id})}
