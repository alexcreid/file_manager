from django.forms import ModelForm

from . import models


class FileUploadForm(ModelForm):
    class Meta:
        model = models.FileUpload
        fields = ['upload', 'is_public']
