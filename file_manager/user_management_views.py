from django.conf import settings
from django.contrib.auth import login, authenticate
import django.contrib.auth.forms as django_auth_forms
from django.shortcuts import redirect
from django.template.response import TemplateResponse
from django.views import View


class UserRegistrationView(View):

    """A simple view that allows basic user registration."""

    _registration_template = 'user_management/user_registration.html'

    def get(self, request, *args, **kwargs):
        return TemplateResponse(
            request, self._registration_template,
            context={'form': self._get_form()})

    def post(self, request, *args, **kwargs):
        registration_form = self._get_form(request.POST)

        if not registration_form.is_valid():
            return TemplateResponse(request, self._registration_template,
                                    context={'form': registration_form})

        # If the form is valid, we can use it to save the user and log them in.
        # This saves the user, but won't log them in:
        registration_form.save()

        # We need to get the user instance via `authenticate`, or they won't
        # have required auth attributes set and things won't be very happy:
        user = authenticate(
            username=registration_form.cleaned_data['username'],
            password=registration_form.cleaned_data['password1'])
        login(request, user)

        return redirect(settings.LOGIN_REDIRECT_URL)

    def _get_form(self, data=None):
        if data is None:
            return django_auth_forms.UserCreationForm()
        else:
            return django_auth_forms.UserCreationForm(data)
