# Python Task -- Alexander Reid
Email: hello@alexreid.org

This Django project should satisfy the requirements for the task set. You can
run it locally as follows (all commands should be issued from the directory
that contains `manage.py`):

```
# Ideally from within a virtualenv running Python 3.7 (although I think 3.6
# should also work)
pip install -r requirements.txt
python manage.py migrate
python manage.py runserver
```

The app should be fairly easy to navigate just by visiting the root URL
(probably http://localhost:8000/ unless further arguments are given to the
runserver command). You'll be prompted to log in or register, and then
redirected to the file list, from which you can upload new files, or log out.

The search API endpoint is available at /files/api/search/. Search strings are
provided via URL query parameters -- for example, getting
/files/api/search/?q=foo&q=bar will search for files that contain both 'foo'
and 'bar'. The results are given as a JSON array of objects containing file
data and download links.

There are a few unit tests for the uploaded file model manager, which can be
run via Django's test runner (`python manage.py test`).

Files by default will be uploaded to user directories that live inside the
`uploaded_files` directory, but this can be customised by altering the
`MEDIA_ROOT` setting.

This is currently using sqlite for database duties for ease of development and
running, but this would probably change if the app were developed further!
